package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthClientEndpoint getAuthEndpoint();

    @NotNull
    IProjectClientEndpoint getProjectEndpoint();

    @NotNull
    ITaskClientEndpoint getTaskEndpoint();

    @NotNull
    IUserClientEndpoint getUserEndpoint();

    @NotNull
    IDomainClientEndpoint getDomainEndpoint();

}
