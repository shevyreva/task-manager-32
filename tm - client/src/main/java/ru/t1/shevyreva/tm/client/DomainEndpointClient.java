package ru.t1.shevyreva.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.IDomainClientEndpoint;
import ru.t1.shevyreva.tm.dto.request.data.*;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.dto.response.data.*;

import java.io.IOException;

@NoArgsConstructor
public class DomainEndpointClient extends AbstractEndpointClient implements IDomainClientEndpoint {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    public @NotNull DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @Override
    public DataLoadBase64Response loadDataBase64(@NotNull final DataLoadBase64Request request) {
        return call(request, DataLoadBase64Response.class);
    }

    @Override
    public DataSaveBase64Response saveDataBase64(@NotNull final DataSaveBase64Request request) {
        return call(request, DataSaveBase64Response.class);
    }

    @Override
    public DataLoadBinaryResponse loadDataBinary(@NotNull final DataLoadBinaryRequest request) {
        return call(request, DataLoadBinaryResponse.class);
    }

    @Override
    public DataSaveBinaryResponse saveDataBinary(@NotNull final DataSaveBinaryRequest request) {
        return call(request, DataSaveBinaryResponse.class);
    }

    @Override
    public DataLoadJsonFasterXmlResponse loadJsonDataFasterXml(@NotNull final DataLoadJsonFasterXmlRequest request) {
        return call(request, DataLoadJsonFasterXmlResponse.class);
    }

    @Override
    public DataLoadJsonJaxBResponse loadJsonDataJaxB(@NotNull final DataLoadJsonJaxBRequest request) {
        return call(request, DataLoadJsonJaxBResponse.class);
    }

    @Override
    public DataSaveJsonFasterXmlResponse saveJsonDataFasterXml(@NotNull final DataSaveJsonFasterXmlRequest request) {
        return call(request, DataSaveJsonFasterXmlResponse.class);
    }

    @Override
    public DataSaveJsonJaxBResponse saveJsonDataJaxB(@NotNull final DataSaveJsonJaxBRequest request) {
        return call(request, DataSaveJsonJaxBResponse.class);
    }

    @Override
    public DataLoadXmlFasterXmlResponse loadXmlDataFasterXml(@NotNull final DataLoadXmlFasterXmlRequest request) {
        return call(request, DataLoadXmlFasterXmlResponse.class);
    }

    @Override
    public DataLoadXmlJaxBResponse loadXmlDataJaxB(@NotNull final DataLoadXmlJaxBRequest request) {
        return call(request, DataLoadXmlJaxBResponse.class);
    }

    @Override
    public DataSaveXmlFasterXmlResponse saveXmlDataFasterXml(@NotNull final DataSaveXmlFasterXmlRequest request) {
        return call(request, DataSaveXmlFasterXmlResponse.class);
    }

    @Override
    public DataSaveXmlJaxBResponse saveXmlDataJaxB(@NotNull final DataSaveXmlJaxBRequest request) {
        return call(request, DataSaveXmlJaxBResponse.class);
    }

    @Override
    public DataLoadYamlFasterXmlResponse loadYamlDataFasterXml(@NotNull final DataLoadYamlFasterXmlRequest request) {
        return call(request, DataLoadYamlFasterXmlResponse.class);
    }

    @Override
    public DataSaveYamlFasterXmlResponse saveYamlDataFasterXml(@NotNull final DataSaveYamlFasterXmlRequest request) {
        return call(request, DataSaveYamlFasterXmlResponse.class);
    }

}
