package ru.t1.shevyreva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.IAuthClientEndpoint;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthClientEndpoint {


    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
