package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskRemoveByProjectIdRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskRemoveByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Remove task by project Id.";

    @NotNull
    private final String NAME = "task-remove-by-project-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().removeTaskByProjectId(new TaskRemoveByProjectIdRequest(id));
    }

}
