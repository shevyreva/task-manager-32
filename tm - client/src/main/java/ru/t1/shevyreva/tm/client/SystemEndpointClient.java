package ru.t1.shevyreva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shevyreva.tm.dto.request.system.ServerAboutRequest;
import ru.t1.shevyreva.tm.dto.request.system.ServerVersionRequest;
import ru.t1.shevyreva.tm.dto.response.system.ServerAboutResponse;
import ru.t1.shevyreva.tm.dto.response.system.ServerVersionResponse;


@NoArgsConstructor
public class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {


    public SystemEndpointClient(@NotNull SystemEndpointClient client) {
        super(client);
    }

    @NotNull
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
