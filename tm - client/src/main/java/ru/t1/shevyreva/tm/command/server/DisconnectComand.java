package ru.t1.shevyreva.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DisconnectComand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";
    @NotNull
    private final String DESCRIPTION = "Disconnect from the server.";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();
    }

    @Override
    public @Nullable Role[] getRoles() {
        return null;
    }

}
