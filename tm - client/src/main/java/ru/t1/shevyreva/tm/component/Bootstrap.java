package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.shevyreva.tm.api.endpoint.*;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.api.service.ILoggerService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.client.*;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shevyreva.tm.exception.system.CommandNotSupportedException;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.service.CommandService;
import ru.t1.shevyreva.tm.service.LoggerService;
import ru.t1.shevyreva.tm.service.PropertyService;
import ru.t1.shevyreva.tm.util.SystemUtil;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.shevyreva.tm.command";
    @NotNull
    @Getter
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    @Getter
    private final FileScanner fileScanner = new FileScanner(this);
    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    @Getter
    private final IAuthClientEndpoint authEndpoint = new AuthEndpointClient();
    @NotNull
    @Getter
    private final IProjectClientEndpoint projectEndpoint = new ProjectEndpointClient();
    @NotNull
    @Getter
    private final IDomainClientEndpoint domainEndpoint = new DomainEndpointClient();
    @NotNull
    @Getter
    private final IUserClientEndpoint userEndpoint = new UserEndpointClient();
    @NotNull
    @Getter
    private final ITaskClientEndpoint taskEndpoint = new TaskEndpointClient();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
    }

    @SneakyThrows
    public void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    public void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println(e.getMessage());
                System.out.println("[FAILED]");
            }
        }
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    public void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("**Welcome to Task Manager**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("**Shutdown Task Manager**");
        fileScanner.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull File file = new File(fileName);
        file.deleteOnExit();
    }

}
