package ru.t1.shevyreva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.dto.request.data.*;
import ru.t1.shevyreva.tm.dto.response.data.*;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator){
        super(serviceLocator);
    }

    @Override
    @NotNull
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @Override
    public DataLoadBase64Response loadDataBase64(@NotNull final DataLoadBase64Request request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @Override
    public DataSaveBase64Response saveDataBase64(@NotNull final DataSaveBase64Request request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @Override
    public DataLoadBinaryResponse loadDataBinary(@NotNull final DataLoadBinaryRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @Override
    public DataSaveBinaryResponse saveDataBinary(@NotNull final DataSaveBinaryRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @Override
    public DataLoadJsonFasterXmlResponse loadJsonDataFasterXml(@NotNull final DataLoadJsonFasterXmlRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadJsonDataFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @Override
    public  DataLoadJsonJaxBResponse loadJsonDataJaxB(@NotNull final DataLoadJsonJaxBRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadJsonDataJaxB();
        return new DataLoadJsonJaxBResponse();
    }

    @Override
    public DataSaveJsonFasterXmlResponse saveJsonDataFasterXml(@NotNull final DataSaveJsonFasterXmlRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveJsonDataFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @Override
    public DataSaveJsonJaxBResponse saveJsonDataJaxB(@NotNull final DataSaveJsonJaxBRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveJsonDataJaxB();
        return new DataSaveJsonJaxBResponse();
    }

    @Override
    public DataLoadXmlFasterXmlResponse loadXmlDataFasterXml(@NotNull final DataLoadXmlFasterXmlRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadXmlDataFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @Override
    public DataLoadXmlJaxBResponse loadXmlDataJaxB(@NotNull final DataLoadXmlJaxBRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadXmlDataJaxB();
        return new DataLoadXmlJaxBResponse();
    }

    @Override
    public DataSaveXmlFasterXmlResponse saveXmlDataFasterXml(@NotNull final DataSaveXmlFasterXmlRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveXmlDataFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @Override
    public DataSaveXmlJaxBResponse saveXmlDataJaxB(@NotNull final DataSaveXmlJaxBRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveXmlDataJaxB();
        return new DataSaveXmlJaxBResponse();
    }

    @Override
    public DataLoadYamlFasterXmlResponse loadYamlDataFasterXml(@NotNull final DataLoadYamlFasterXmlRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadYamlDataFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @Override
    public DataSaveYamlFasterXmlResponse saveYamlDataFasterXml(@NotNull final DataSaveYamlFasterXmlRequest request){
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveYamlDataFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }

}
