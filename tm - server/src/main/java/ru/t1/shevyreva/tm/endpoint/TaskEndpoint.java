package ru.t1.shevyreva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.dto.request.task.*;
import ru.t1.shevyreva.tm.dto.response.task.*;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Task;

import javax.validation.constraints.Null;
import java.util.List;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService(){
        return getServiceLocator().getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService(){
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable Task task =  getTaskService().changeTaskStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable Task task =  getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    public @NotNull TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().removeAll(userId);
        return new TaskClearResponse();
    }

    @Override
    public @NotNull TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    public @NotNull TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable Task task = getTaskService().findOneById(userId, taskId);
        return new TaskGetByIdResponse(task);
    }

    @Override
    public @NotNull TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @Override
    public @NotNull TaskListResponse listTask(@NotNull TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final TaskSort sort = request.getSort();
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @Override
    public @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getId();
        getTaskService().removeOneById(userId, taskId);
        return null;
    }

    @Override
    public @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task = getTaskService().removeOneByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    public @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateByIndex(userId,index,name,description);
        return new TaskUpdateByIndexResponse(task);
    }

    @Override
    public @NotNull TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @Override
    public @NotNull TaskUnbindToProjectResponse unbindTaskFromProject(@NotNull TaskUnbindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskUnbindToProjectResponse();
    }

    @Override
    public @NotNull TaskRemoveByProjectIdResponse removeTaskByProjectId(@NotNull TaskRemoveByProjectIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        getProjectTaskService().removeByProjectId(userId, projectId);
        return new TaskRemoveByProjectIdResponse();
    }

    @Override
    public @NotNull TaskLisByProjectIdResponse listTaskByProjectId(@NotNull TaskListByProjectIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> tasks = getProjectTaskService().findAllTaskByProjectId(userId, projectId);
        return new TaskLisByProjectIdResponse(tasks);
    }

}
