package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.*;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.dto.request.data.*;
import ru.t1.shevyreva.tm.dto.request.project.*;
import ru.t1.shevyreva.tm.dto.request.system.ServerAboutRequest;
import ru.t1.shevyreva.tm.dto.request.system.ServerVersionRequest;
import ru.t1.shevyreva.tm.dto.request.task.*;
import ru.t1.shevyreva.tm.dto.request.user.*;
import ru.t1.shevyreva.tm.endpoint.*;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;
import ru.t1.shevyreva.tm.repository.UserRepository;
import ru.t1.shevyreva.tm.service.*;
import ru.t1.shevyreva.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);
    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.shevyreva.tm.command";
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);
    @NotNull
    private final Server server = new Server(this);
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(UserRegistryRequest.class, userEndpoint::registry);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfile);
        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataLoadBase64Request.class, domainEndpoint::loadDataBase64);
        server.registry(DataSaveBase64Request.class, domainEndpoint::saveDataBase64);
        server.registry(DataLoadBinaryRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataSaveBinaryRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataLoadJsonFasterXmlRequest.class, domainEndpoint::loadJsonDataFasterXml);
        server.registry(DataLoadJsonJaxBRequest.class, domainEndpoint::loadJsonDataJaxB);
        server.registry(DataSaveJsonFasterXmlRequest.class, domainEndpoint::saveJsonDataFasterXml);
        server.registry(DataSaveJsonJaxBRequest.class, domainEndpoint::saveJsonDataJaxB);
        server.registry(DataLoadXmlFasterXmlRequest.class, domainEndpoint::loadXmlDataFasterXml);
        server.registry(DataLoadXmlJaxBRequest.class, domainEndpoint::loadXmlDataJaxB);
        server.registry(DataSaveXmlFasterXmlRequest.class, domainEndpoint::saveXmlDataFasterXml);
        server.registry(DataSaveXmlJaxBRequest.class, domainEndpoint::saveXmlDataJaxB);
        server.registry(DataLoadYamlFasterXmlRequest.class, domainEndpoint::loadYamlDataFasterXml);
        server.registry(DataSaveYamlFasterXmlRequest.class, domainEndpoint::saveYamlDataFasterXml);

        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getProjectById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getProjectByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getTaskById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getTaskByIndex);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindToProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskRemoveByProjectIdRequest.class, taskEndpoint::removeTaskByProjectId);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTaskByProjectId);
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("**Welcome to Task Manager Server**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("**Shutdown Task Manager Server**");
        backup.stop();
        server.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        userService.create("user", "user", "user@gmail.com");
        userService.create("test", "test", "test@yandex.ru");
        userService.create("admin", "admin", Role.ADMIN);

        @NotNull final String userTestId = userService.findByLogin("test").getId();
        @NotNull final String userAdminId = userService.findByLogin("admin").getId();

        projectService.add(userTestId, new Project("B PROJECT", Status.COMPLETED));
        projectService.add(userTestId, new Project("A PROJECT", Status.IN_PROGRESS));
        projectService.add(userTestId, new Project("D PROJECT", Status.NON_STARTED));
        projectService.add(userAdminId, new Project("C PROJECT", Status.IN_PROGRESS));

        taskService.add(userTestId, new Task("MEGA TASK", Status.COMPLETED));
        taskService.add(userAdminId, new Task("SUPER TASK", Status.NON_STARTED));
    }

}
