package ru.t1.shevyreva.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.component.Server;
import ru.t1.shevyreva.tm.dto.request.AbstractRequest;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;
import ru.t1.shevyreva.tm.dto.response.ApplicationErrorResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserProfileResponse;
import ru.t1.shevyreva.tm.model.User;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;


public final class ServerRequestTask extends AbstractServerSocketTask {

    @Nullable
    private AbstractRequest request;

    @Nullable
    private AbstractResponse response;

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server, socket);
    }

    public ServerRequestTask(
        @NotNull final Server server,
        @NotNull final Socket socket,
        @Nullable final String userId
    ) {
        super(server, socket, userId);
    }

    @Override
    @SneakyThrows
    public void run() {
        processInput(); //принятие запроса любого, неважно нужен userId или нет

        processUserId();//здесь нужен userId, если это запрос без него, то выполняться не будет
        processUserId();
        processLogin();
        processProfile();
        processLogout();

        processOperation();
        processOutput();
    }

    private void processUserId(){
        if(!(request instanceof AbstractUserRequest)) return;
        @NotNull final AbstractUserRequest abstractUserRequest = (AbstractUserRequest) request;
        abstractUserRequest.setUserId(userId);
    }

    //принятие запроса на сервер от клиента
    @SneakyThrows
    private void processInput(){
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull Object object = objectInputStream.readObject();
        request = (AbstractRequest) object;
    }

    //отдаем объект от сервера
    @SneakyThrows
    private void processOutput(){
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

    @SneakyThrows
    private void processLogin(){
        if (response != null) return;
        if (!(request instanceof UserLoginRequest)) return;
        try{
            @NotNull UserLoginRequest userLoginRequest = (UserLoginRequest) request;
            @Nullable final String login = userLoginRequest.getLogin();
            @Nullable final String password = userLoginRequest.getPassword();
            @NotNull final IAuthService authService = server.getBootstrap().getAuthService();
            @NotNull final User user = authService.check(login, password);
            userId = user.getId();
            response = new UserLoginResponse();
        }
        catch(@NotNull final Exception e){
            response = new ApplicationErrorResponse(e);
        }
    }


    private void processProfile(){
        if (response != null) return;
        if (!(request instanceof UserProfileRequest)) return;
        if (userId == null){
            response = new UserProfileResponse();
            return;
        }
        @NotNull final IUserService userService = server.getBootstrap().getUserService();
        @NotNull final User user = userService.findOneById(userId);
        response = new UserProfileResponse(user);

    }

    private void processLogout(){
        if (response != null) return;
        try {
            if (!(request instanceof UserLogoutRequest)) return;
            userId = null;
            response = new UserLogoutResponse();
        }
        catch (@NotNull final  Exception e){
            response = new ApplicationErrorResponse(e);
        }
    }

    private void processOperation(){
        if (response != null) return;
        try{
            if (request == null) return;
            @Nullable final Object result = server.call(request);
            response = (AbstractResponse) result;
        }
        catch(@NotNull final Exception e){
            response = new ApplicationErrorResponse(e);
        }
    }

}
