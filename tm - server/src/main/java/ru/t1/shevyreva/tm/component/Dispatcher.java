package ru.t1.shevyreva.tm.component;


import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.IOperation;
import ru.t1.shevyreva.tm.dto.request.AbstractRequest;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, IOperation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final IOperation<RQ, RS> IOperation
    ) {
        map.put(reqClass, IOperation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        @NotNull final IOperation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
