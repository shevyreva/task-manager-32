package ru.t1.shevyreva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shevyreva.tm.api.service.IProjectService;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.dto.request.project.*;
import ru.t1.shevyreva.tm.dto.response.project.*;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService(){
        return getServiceLocator().getProjectService();
    }

    @Override
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    public @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    public @NotNull ProjectClearResponse clearProject(
            @NotNull ProjectClearRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().removeAll(userId);
        return new ProjectClearResponse();
    }

    @Override
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project =  getProjectService().create(userId,name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    public ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project =  getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @Override
    public ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project =  getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @Override
    public ProjectListResponse listProject(@NotNull ProjectListRequest request) {
        check(request);
        @Nullable final String userId =  request.getUserId();
        @Nullable final ProjectSort sort = request.getSort();
        @NotNull final List<Project> projects =  getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project =  getProjectService().removeOneById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project =  getProjectService().removeOneByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String id = request.getId();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project =  getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project =  getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
