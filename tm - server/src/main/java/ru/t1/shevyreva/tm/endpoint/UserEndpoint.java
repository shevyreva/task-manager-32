package ru.t1.shevyreva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IUserEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.dto.request.user.*;
import ru.t1.shevyreva.tm.dto.response.user.*;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @SneakyThrows
    public @NotNull UserRegistryResponse registry(@NotNull UserRegistryRequest request) {
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        @NotNull User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @Override
    public @NotNull UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request) {
        check(request);
        @Nullable String password = request.getPassword();
        @Nullable String userId = request.getUserId();
        @NotNull User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    public @NotNull UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String firstName = request.getFirstName();
        @Nullable String lastName = request.getLastName();
        @Nullable String middleName = request.getMiddleName();
        @NotNull User user = getUserService().updateUser(userId, firstName, middleName, lastName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    public @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().lockUser(login);
        return new UserLockResponse(user);
    }

    @Override
    public @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getUserService().unlockUser(login);
        return new UserUnlockResponse(user);
    }

    @Override
    public @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final String email = request.getEmail();
        @Nullable final String id = request.getUserId();
        @Nullable final User user;
        if (login != null) user = getUserService().removeByLogin(login);
        else if (email != null) user = getUserService().removeByEmail(email);
        else if (id != null) user = getUserService().removeOneById(id);
        else return new UserRemoveResponse(null);
        return new UserRemoveResponse(user);
    }

}
