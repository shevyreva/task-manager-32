package ru.t1.shevyreva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.data.*;
import ru.t1.shevyreva.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    DataLoadBase64Response loadDataBase64(@NotNull DataLoadBase64Request request);

    DataSaveBase64Response saveDataBase64(@NotNull DataSaveBase64Request request);

    DataLoadBinaryResponse loadDataBinary(@NotNull DataLoadBinaryRequest request);

    DataSaveBinaryResponse saveDataBinary(@NotNull DataSaveBinaryRequest request);

    DataLoadJsonFasterXmlResponse loadJsonDataFasterXml(@NotNull DataLoadJsonFasterXmlRequest request);

    DataLoadJsonJaxBResponse loadJsonDataJaxB(@NotNull DataLoadJsonJaxBRequest request);

    DataSaveJsonFasterXmlResponse saveJsonDataFasterXml(@NotNull DataSaveJsonFasterXmlRequest request);

    DataSaveJsonJaxBResponse saveJsonDataJaxB(@NotNull DataSaveJsonJaxBRequest request);

    DataLoadXmlFasterXmlResponse loadXmlDataFasterXml(@NotNull DataLoadXmlFasterXmlRequest request);

    DataLoadXmlJaxBResponse loadXmlDataJaxB(@NotNull DataLoadXmlJaxBRequest request);

    DataSaveXmlFasterXmlResponse saveXmlDataFasterXml(@NotNull DataSaveXmlFasterXmlRequest request);

    DataSaveXmlJaxBResponse saveXmlDataJaxB(@NotNull DataSaveXmlJaxBRequest request);

    DataLoadYamlFasterXmlResponse loadYamlDataFasterXml(@NotNull DataLoadYamlFasterXmlRequest request);

    DataSaveYamlFasterXmlResponse saveYamlDataFasterXml(@NotNull DataSaveYamlFasterXmlRequest request);

}
