package ru.t1.shevyreva.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Task;

public class TaskGetByIndexResponse extends AbstractTaskResponse{

    public TaskGetByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
