package ru.t1.shevyreva.tm.dto.response.task;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

@Getter
public class TaskLisByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskLisByProjectIdResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }
    
}
