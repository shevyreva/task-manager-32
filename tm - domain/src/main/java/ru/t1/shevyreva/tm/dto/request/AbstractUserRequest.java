package ru.t1.shevyreva.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractRequest;

@Getter
@Setter
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;

}
