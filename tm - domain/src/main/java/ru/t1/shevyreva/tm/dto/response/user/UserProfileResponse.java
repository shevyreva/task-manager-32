package ru.t1.shevyreva.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.User;

public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(){}

    public UserProfileResponse(@Nullable User user) {
        super(user);
    }

}
