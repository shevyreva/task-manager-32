package ru.t1.shevyreva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.user.*;
import ru.t1.shevyreva.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull
    UserRegistryResponse registry(@NotNull UserRegistryRequest request);

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

}
