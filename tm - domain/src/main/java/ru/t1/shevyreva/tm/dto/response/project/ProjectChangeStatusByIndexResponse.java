package ru.t1.shevyreva.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse{

    public ProjectChangeStatusByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
