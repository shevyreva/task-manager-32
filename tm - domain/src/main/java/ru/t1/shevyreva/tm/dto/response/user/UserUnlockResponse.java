package ru.t1.shevyreva.tm.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.User;

public class UserUnlockResponse extends AbstractUserResponse{

    public UserUnlockResponse(@NotNull User user) {
        super(user);
    }

}
