package ru.t1.shevyreva.tm.dto.response.user;

import ru.t1.shevyreva.tm.model.User;

public class UserUpdateProfileResponse extends AbstractUserResponse{

    public UserUpdateProfileResponse(User user) {
        super(user);
    }

}
