package ru.t1.shevyreva.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;

public class ProjectRemoveByIdResponse extends AbstractProjectResponse{

    public ProjectRemoveByIdResponse(@Nullable Project project) {
        super(project);
    }

}
