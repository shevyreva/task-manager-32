package ru.t1.shevyreva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.*;
import ru.t1.shevyreva.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request);

    @NotNull
    TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest  request);

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindToProjectResponse unbindTaskFromProject(@NotNull TaskUnbindToProjectRequest request);

    @NotNull
    TaskRemoveByProjectIdResponse removeTaskByProjectId(@NotNull TaskRemoveByProjectIdRequest request);

    @NotNull
    TaskLisByProjectIdResponse listTaskByProjectId(@NotNull TaskListByProjectIdRequest request);

}
