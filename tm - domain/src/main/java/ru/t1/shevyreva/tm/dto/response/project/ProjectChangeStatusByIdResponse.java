package ru.t1.shevyreva.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;

@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse{

    public ProjectChangeStatusByIdResponse(@Nullable Project project) {
        super(project);
    }

}
