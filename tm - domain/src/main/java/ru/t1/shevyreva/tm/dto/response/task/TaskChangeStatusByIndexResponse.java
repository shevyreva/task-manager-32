package ru.t1.shevyreva.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Task;

public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse{

    public TaskChangeStatusByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
