package ru.t1.shevyreva.tm.api.endpoint;

import ru.t1.shevyreva.tm.dto.request.AbstractRequest;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface IOperation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
