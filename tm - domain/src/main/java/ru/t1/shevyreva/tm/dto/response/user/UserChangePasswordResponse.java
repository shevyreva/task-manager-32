package ru.t1.shevyreva.tm.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.model.User;

public class UserChangePasswordResponse extends AbstractUserResponse{

    public UserChangePasswordResponse(@NotNull User user) {
        super(user);
    }

}
