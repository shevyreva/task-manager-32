package ru.t1.shevyreva.tm.dto.request.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
public class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskGetByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}
