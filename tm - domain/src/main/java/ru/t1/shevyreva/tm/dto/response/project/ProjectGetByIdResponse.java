package ru.t1.shevyreva.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;

public class ProjectGetByIdResponse extends AbstractProjectResponse{

    public ProjectGetByIdResponse(@Nullable Project project) {
        super(project);
    }

}
