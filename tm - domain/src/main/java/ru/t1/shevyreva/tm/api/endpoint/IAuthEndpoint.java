package ru.t1.shevyreva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserProfileResponse;

import java.net.Socket;

public interface IAuthEndpoint{

    UserLoginResponse login(@NotNull UserLoginRequest request);

    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    UserProfileResponse profile(@NotNull UserProfileRequest request);

    //Socket getSocket();
}
